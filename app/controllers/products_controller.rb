class ProductsController < ApplicationController
  def show
    @product = Product.first
    @review = Review.new
  end
end
