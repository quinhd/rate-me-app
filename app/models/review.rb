class Review < ApplicationRecord
  include Turbo::Broadcastable
  include Reviews::Broadcastable

  belongs_to :product
  validates :text, presence: true
  validates :rating, inclusion: { in: 0..10 }
end
