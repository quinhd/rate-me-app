module Reviews
  module Broadcastable
    extend ActiveSupport::Concern

    included do
      after_create_commit :broadcast_created_review, :broadcast_product_average
    end

    def broadcast_created_review
      broadcast_append_to([product, :reviews], target: "reviews-container")
    end

    def broadcast_product_average
      broadcast_replace_to([product, :reviews], target: "avg-rating-container",
                                                partial: "reviews/avg_rating",
                                                locals: { rating_avg: product.average_rating })
    end
  end
end
