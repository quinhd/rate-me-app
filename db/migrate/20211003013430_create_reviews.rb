class CreateReviews < ActiveRecord::Migration[6.1]
  def change
    create_table :reviews do |t|
      t.references :product
      t.integer :rating, null: false
      t.string :text

      t.timestamps
    end
  end
end
