module ReviewsHelper
  class ActionView::Helpers::FormBuilder
    include ActionView::Helpers::FormTagHelper
    include ActionView::Helpers::FormOptionsHelper

    def stars_rating_input_tag(name, filled)
      radio_buttons =
        10.downto(1).reduce("") do |result, time|
          input_name = "#{object_name}[#{name}]"
          half_class = "half" if time.odd?

          result << radio_button_tag(input_name, time, time == filled)
          result << label_tag("review_#{name}_#{time}", "", class: "rate-icon #{half_class}")
        end.html_safe

      content_tag(:fieldset, radio_buttons, class: "rate rate--clickable")
    end
  end

  def stars_rating(filled)
    rating_icons =
      10.downto(1).reduce("") do |result, time|
        filled_class = "filled" if time <= filled.round
        half_class = "half" if time.odd?

        result << label_tag(nil, "", class: "rate-icon #{filled_class} #{half_class}")
      end.html_safe

    content_tag(:fieldset, rating_icons, class: "rate")
  end

  def format_rating(value)
    # 2.0 is the conversion rate. Because the rating in db is a value from 1 to 10
    # and in the presentation is a value from 1 to 5
    number_with_precision(value / 2.0, precision: 2, strip_insignificant_zeros: true)
  end
end
