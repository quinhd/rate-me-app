Rails.application.routes.draw do
  root to: "products#show"
  resources :products, only: [:show] do
    resources :reviews, only: %i[index create]
  end
end
