class ReviewsController < ApplicationController
  before_action :set_product

  def create
    @review = @product.reviews.new(review_params)
    if @review.save
      respond_to do |format|
        format.turbo_stream { redirect_to(@product) }
      end
    else
      respond_to do |format|
        format.turbo_stream do
          render turbo_stream:
          turbo_stream.replace("new_review_form",
                               partial: "reviews/form",
                               locals: { review: @review, product: @product })
        end
      end
    end
  end

  private

  def set_product
    @product = Product.find(params[:product_id])
  end

  def review_params
    params.require(:review).permit(:rating, :text)
  end
end
