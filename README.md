# README

This is a single-product rating app that allows to send written reviews 

# Start up

Clone this repo

Run:
`rails db:create db:migrate db:seed`

After this, there'll be a product with 3 reviews.

Then, to start the server:
`rails s`

And now, you can go to:
`http://localhost:3000/`


# How to use it
This is the product view. It shows the product's name, and its average rating. The stars value is rounded.
![plot](https://i.ibb.co/XDfYfMS/Captura-de-pantalla-2021-10-05-a-las-12-06-51.png)

Clicking in _add review_ button it prompts a modal window to add a new review                                     
![plot](https://i.ibb.co/BtChPG0/Captura-de-pantalla-2021-10-05-a-las-12-41-09.png)

If any field is wrong when the form is submitted, it shows the validation errors.                                     
![plot](https://i.ibb.co/vYK5ddH/Captura-de-pantalla-2021-10-05-a-las-12-08-09.png)

When the fields are properly filled, the form can be submitted and the new review is created                                     
![plot](https://i.ibb.co/YcN3S7X/Captura-de-pantalla-2021-10-05-a-las-12-34-59.png)
![plot](https://i.ibb.co/3TGpwCK/Captura-de-pantalla-2021-10-05-a-las-12-35-10.png)


# How it works
* The creation and display of the product's rating and the errors in the form are made through Turbo. So thanks to this technology, the view only reloads the parts it needs.
* When a new review is created (using the web form or using the rails console). The app broadcasts it to every page that is showing the product. Once again, this is possible thanks to Turbo.
