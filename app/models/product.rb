class Product < ApplicationRecord
  has_many :reviews, dependent: :destroy
  validates :name, presence: true

  def average_rating
    reviews.average(:rating).round(2)
  end
end
